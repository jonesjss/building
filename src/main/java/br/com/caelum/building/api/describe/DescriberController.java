package br.com.caelum.building.api.describe;

import lombok.AllArgsConstructor;
import lombok.Data;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@AllArgsConstructor
class DescriberController {
    private DescribeService service;

    @GetMapping
    List<BuildingOutput> list(@RequestParam(required = false) List<Long> buildingIds) {
        return service.listAll(buildingIds);
    }

    @GetMapping("{id}")
    BuildingOutput show(@PathVariable Long id, @RequestParam Long correlationalId) {
       return service.showBy(id);
    }

    @Data
    @AllArgsConstructor
    static class BuildingOutput {
        private Long id;
        private String condominiumName;
        private String address;
        private Integer numberOfBlocks;
        private Integer numberOfUnits;
        private final String realEstateAgent = "Fernando";
    }
}
