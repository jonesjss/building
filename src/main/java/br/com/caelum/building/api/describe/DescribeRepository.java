package br.com.caelum.building.api.describe;

import br.com.caelum.building.domain.Building;

import java.util.List;
import java.util.Optional;

public interface DescribeRepository {
    List<Building> findAll();

    Optional<Building> findById(Long id);

    List<Building> finaAllWhereIdsIn(List<Long> buildingIds);
}
