package br.com.caelum.building.api.registration;

import br.com.caelum.building.domain.Building;

public interface RegistrationRepository {
    void save(Building building);
}
