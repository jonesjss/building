package br.com.caelum.building.api.registration;

import br.com.caelum.building.domain.Building;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

@Service
@AllArgsConstructor
class RegistrationService {
    private final RegistrationRepository buildings;
    private BuildingInputToBuilding mapper;

    Long registerBy(RegistrationController.BuildingInput input) {
        Building building = mapper.map(input);

        buildings.save(building);

        return building.getId();
    }
}
