package br.com.caelum.building.listener.remote;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.extern.slf4j.Slf4j;
import org.springframework.cloud.stream.annotation.EnableBinding;
import org.springframework.cloud.stream.annotation.StreamListener;
import org.springframework.cloud.stream.messaging.Sink;
import org.springframework.stereotype.Component;

@Slf4j
@Component
@EnableBinding(Sink.class)
@AllArgsConstructor
public class ApartmentListener {

    private final AddApartmentService service;

    @StreamListener(Sink.INPUT)
    void handle(CreatedApartment apartment) {
        log.info("Receive a new event: {}", apartment);

        service.incrementApartmentOf(apartment.getBuildingId());
    }


    @Data
    static class CreatedApartment {
        private Long buildingId;
    }
}
