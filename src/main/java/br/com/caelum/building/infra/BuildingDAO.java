package br.com.caelum.building.infra;

import br.com.caelum.building.domain.Building;
import org.springframework.data.repository.Repository;

import java.util.List;
import java.util.Optional;

public interface BuildingDAO extends Repository<Building, Long> {
    List<Building> findAll();

    List<Building> findAllByIdIn(List<Long> ids);

    Optional<Building> findById(Long id);

    void save(Building building);
}
