package br.com.caelum.building.infra;

import lombok.AllArgsConstructor;
import lombok.Getter;

import java.time.LocalDateTime;

@Getter
@AllArgsConstructor
public class GenericErrorPayload {
    private final LocalDateTime date = LocalDateTime.now();
    private String error;
}
