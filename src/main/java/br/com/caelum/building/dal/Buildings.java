package br.com.caelum.building.dal;

import br.com.caelum.building.api.describe.DescribeRepository;
import br.com.caelum.building.api.registration.RegistrationRepository;
import br.com.caelum.building.domain.Building;
import br.com.caelum.building.infra.BuildingDAO;
import br.com.caelum.building.listener.remote.AddApartmentRepository;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Optional;

@Component
@AllArgsConstructor
class Buildings implements RegistrationRepository, DescribeRepository, AddApartmentRepository {
    private BuildingDAO repository;

    @Override
    public void save(Building building) {
        repository.save(building);
    }

    @Override
    public List<Building> findAll() {
        return repository.findAll();
    }

    @Override
    public Optional<Building> findById(Long id) {
        return repository.findById(id);
    }

    @Override
    public List<Building> finaAllWhereIdsIn(List<Long> buildingIds) {
        return repository.findAllByIdIn(buildingIds);
    }
}
