package br.com.caelum.building.shared;

public interface Mapper<S, T> {
    T map(S source);
}
